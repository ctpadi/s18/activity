function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;
	this.isFainted = false;
	//methods
	this.tackle = function(target) {
		console.log(this.name + ' tackled '+ target.name);
		target.health = target.health - this.attack
		if (target.health < 0) target.health = 0;
		if (!target.isFainted) console.log(target.name + "'s health is now reduced to " + target.health);
		else console.log(target.name + " died LMAO"); //stretch goals :)
		if (target.health <= 5 && !target.isFainted){
			target.faint();
			target.isFainted = true;
		}
	},
	this.faint = function() {
		console.log(this.name + " fainted")
	}
}

//Creates a new instance of the "Pokemon" object each with their unique properties
let charmander = new Pokemon("Charmander", 20);
let bulbasaur = new Pokemon("Bulbasaur", 19);

charmander.tackle(bulbasaur);
charmander.tackle(bulbasaur);
charmander.tackle(bulbasaur);
charmander.tackle(bulbasaur);
charmander.tackle(bulbasaur);
charmander.tackle(bulbasaur);